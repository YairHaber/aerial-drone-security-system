from tkinter import *
import cv2
import PIL.Image, PIL.ImageTk
import webbrowser
import subprocess
from time import time
import numpy as np
from darkflow.net.build import TFNet
import random
import string
import socket
import queue

SYS_FONT = 'Google Sans'
IP_FONT = 'Source Code Pro'
HEADLINE_SIZE = 42
SECOND_HEADLINE_SIZE = 26
TEXT_SIZE = 16
TIMER_SIZE = 80

AERIAL_BLUE = '#BDE3F2'
AERIAL_PURPLE = '#353A67'
CODE_GRAY = '#CCD0F6'

WINDOW_MIN_DIMENSIONS = '800x540'
MAIN_WINDOW_DIMENSIONS = '1200x650'


class DockerServer:
    def __init__(self):
        print(subprocess.getstatusoutput('docker ps -a'))


class MissionTimer:
    def __init__(self, parent):
        self.display = Label(parent, text='00:00', font=(SYS_FONT, TIMER_SIZE), bg=AERIAL_BLUE,
                             fg=AERIAL_PURPLE)
        self.display.place(x=10, y=350)
        self.oldtime = time()
        self.run_timer()

    def run_timer(self):
        global timestr
        delta = int(time() - self.oldtime)
        timestr = '{:02}:{:02}'.format(*divmod(delta, 60))
        self.display.config(text=timestr)
        self.display.after(1000, self.run_timer)


class TkStream:
    def __init__(self, window, refer_btn, refer_lbl, log_frm, refer_timer,
                 video_source='rtmp://localhost/live/app'):  # 'rtmp://ip/live/id'
        self.window = window
        self.video_source = video_source
        self.frame_index = 0

        MissionTimer(window)

        refer_btn.destroy()
        refer_lbl.destroy()
        refer_timer.destroy()
        self.is_stream_running = True

        # open video source (by default this will try to open the computer webcam)
        self.vid = StreamHandle(self.video_source)

        # Create a canvas that can fit the above video source size
        self.stream_canvas = Canvas(window, width=self.vid.width, height=self.vid.height, bg=AERIAL_BLUE)
        self.stream_canvas.place(x=350, y=70)

        stop_stream_btn = Button(window, text="Stop Stream", fg=AERIAL_PURPLE, font=(SYS_FONT, SECOND_HEADLINE_SIZE),
                                 command=self.stop_stream)
        stop_stream_btn.place(x=55, y=130)

        snapshot_btn = Button(window, text="Take Snapshot", fg=AERIAL_PURPLE, font=(SYS_FONT, SECOND_HEADLINE_SIZE),
                              command=self.take_snapshot)
        snapshot_btn.place(x=40, y=240)

        detection_log = Text(log_frm, height=6, width=110)
        vsb = Scrollbar(log_frm, orient="vertical", command=detection_log.yview)
        detection_log.configure(yscrollcommand=vsb.set)
        vsb.pack(side="right", fill="y")
        detection_log.pack(side="left", fill="both", expand=True)

        live_stream_lbl = Label(window, text='Now Live', font=(SYS_FONT, SECOND_HEADLINE_SIZE), bg=AERIAL_BLUE,
                                fg=AERIAL_PURPLE)
        live_stream_lbl.place(x=1040, y=30)

        # After it is called once, the update method will be automatically called every delay milliseconds
        self.delay = 15
        self.update(window, detection_log)

        self.window.mainloop()

    def boxing(self, original_img, predictions, window, detection_log):
        newImage = np.copy(original_img)

        for result in predictions:
            # Getting the quordinates of the topleft in detection
            top_x = result['topleft']['x']
            top_y = result['topleft']['y']

            btm_x = result['bottomright']['x']
            btm_y = result['bottomright']['y']

            # Getting the confidence of the neural net in this detection
            confidence = result['confidence']
            # Assembling a label to insert to the detection log
            label = result['label'] + " " + str(round(confidence, 3))

            if confidence > 0.3:
                if label[:label.find(" ")] == 'person' or label[:label.find(" ")] == 'car':
                    # Drawing the rectangle
                    newImage = cv2.rectangle(newImage, (top_x, top_y), (btm_x, btm_y), (255, 0, 0), 3)
                    labels = ""
                    if confidence > 0.3:
                        labels = ""
                        for prediction in predictions:
                            detection = prediction['label']
                            if detection == 'person' or detection == 'car':
                                percentage = str(prediction['confidence'])[2:4] + "%"
                                label = detection + " " + percentage
                                labels = labels + ", " + label

        detection_log.insert("end", "On " + timestr + ": " + labels + "\n")
        detection_log.see("end")

        return newImage

    def update(self, window, detection_log):
        # Get a frame from the video source
        try:
            ret, frame_queue = self.vid.get_frame()
        except:
            pass
        try:
            if ret:
                frame = np.asarray(frame_queue.get())
                self.frame_index += 1

                results = tfnet.return_predict(frame)

                self.new_frame = self.boxing(frame, results, window, detection_log)

                self.photo = PIL.ImageTk.PhotoImage(image=PIL.Image.fromarray(self.new_frame))
                self.stream_canvas.create_image(0, 0, image=self.photo, anchor=NW)
        except UnboundLocalError:
            pass

        if self.is_stream_running:
            self.window.after(self.delay, lambda: self.update(window, detection_log))

    def take_snapshot(self):
        cv2.imwrite("Snapshot_Frame %d.jpg" % self.frame_index, self.new_frame)

    def stop_stream(self):
        self.is_stream_running = False
        StreamHandle.__del__(self.vid)


class StreamHandle:
    def __init__(self, video_source):
        # Open the video source
        self.vid = cv2.VideoCapture(video_source)
        self.frame_queue = queue.Queue()
        if not self.vid.isOpened():
            raise ValueError("Unable to open video source", video_source)

        # Get video source width and height
        self.width = 800
        self.height = 425

    def get_frame(self):
        if self.vid.isOpened():
            ret, frame = self.vid.read()

            if ret:
                frame = cv2.resize(frame, (800, 425), fx=0, fy=0, interpolation=cv2.INTER_CUBIC)
                if not self.frame_queue.empty():
                    try:
                        self.q.get_nowait()  # discard previous (unprocessed) frame
                    except queue.Empty:
                        pass
                self.frame_queue.put(frame)
                # Return a boolean success flag and the current frame converted to BGR
                return ret, self.frame_queue
            else:
                return ret, None
        else:
            return None

    # Release the video source when the object is destroyed
    def __del__(self):
        if self.vid.isOpened():
            self.vid.release()


class WelcomeScreen(Frame):
    def __init__(self, parent):
        Frame.__init__(self, parent, bg=AERIAL_BLUE)

        top_banner_img = PhotoImage(file='top-banner.gif')
        top_banner_lbl = Label(self, image=top_banner_img, bg=AERIAL_BLUE)
        top_banner_lbl.photo = top_banner_img
        top_banner_lbl.pack()

        aersoph_ls_logo_img = PhotoImage(file='aersoph_and_ls.gif')
        aersoph_ls_logo_lbl = Label(self, image=aersoph_ls_logo_img, bg=AERIAL_BLUE)
        aersoph_ls_logo_lbl.photo = aersoph_ls_logo_img
        aersoph_ls_logo_lbl.place(x=665, y=0)

        space_lbl = Label(self, text="\n", fg=AERIAL_PURPLE, bg=AERIAL_BLUE, font=(SYS_FONT, 5))
        space_lbl.pack()

        server_ip_lbl = Label(self, text="Server IP")
        server_ip_lbl.config(font=(SYS_FONT, HEADLINE_SIZE, "bold"), fg=AERIAL_PURPLE, bg=AERIAL_BLUE)
        server_ip_lbl.place(x=28, y=250)

        server_ip_entry = Entry(self, justify=CENTER, fg=AERIAL_PURPLE, bg=AERIAL_BLUE,
                                font=(SYS_FONT, SECOND_HEADLINE_SIZE))
        server_ip_entry.place(x=30, y=300)

        stream_id_lbl = Label(self, text="Stream ID")
        stream_id_lbl.config(font=(SYS_FONT, HEADLINE_SIZE, "bold"), fg=AERIAL_PURPLE, bg=AERIAL_BLUE)
        stream_id_lbl.place(x=420, y=250)

        stream_id_entry = Entry(self, justify=CENTER, fg=AERIAL_PURPLE, bg=AERIAL_BLUE,
                                font=(SYS_FONT, SECOND_HEADLINE_SIZE))
        stream_id_entry.place(x=420, y=300)

        space_lbl = Label(self, text="", fg=AERIAL_PURPLE, bg=AERIAL_BLUE)
        space_lbl.pack()

        verify_btn = Button(self, text="Verify", fg=AERIAL_PURPLE, font=(SYS_FONT, SECOND_HEADLINE_SIZE),
                            command=self.start_main_screen)
        verify_btn.place(x=360, y=370)

        verify_btn = Button(self, text=" ? ", fg=AERIAL_PURPLE, font=(SYS_FONT, SECOND_HEADLINE_SIZE, "bold"),
                            command=self.help)
        verify_btn.place(x=10, y=490)

        space_lbl = Label(self, text="\n", fg=AERIAL_PURPLE, bg=AERIAL_BLUE)
        space_lbl.pack()

        tutorial_btn = Button(self, text="I'm new here", command=self.start_tutorial, fg=AERIAL_PURPLE,
                              font=(SYS_FONT, SECOND_HEADLINE_SIZE))
        tutorial_btn.place(x=630, y=490)

        self.parent = parent

    def help(self):
        help_lbl = Label(self, text="In these two fields please insert the server IP address and your randomized "
                                    "stream ID.\n If you are here for the first time, click \"I'm new here\" "
                                    "to go through a tutorial.", fg=AERIAL_PURPLE, bg=AERIAL_BLUE,
                         font=(SYS_FONT, TEXT_SIZE))
        help_lbl.place(x=70, y=430)

    def clear_frame(self):
        for widget in self.winfo_children():
            widget.destroy()

    def start_tutorial(self):
        self.clear_frame()
        Tutorial(root, self).pack(fill="both", expand=True)

    def start_main_screen(self):
        self.clear_frame()
        MainScreen(root, self).pack(fill="both", expand=True)


class Tutorial(Frame):
    def __init__(self, parent, referer):
        try:
            referer.destroy()
        except:
            pass

        Frame.__init__(self, parent, bg=AERIAL_BLUE)

        stuff_needed_head_lbl = Label(self, text='The things you\'ll need', font=(SYS_FONT, HEADLINE_SIZE),
                                      fg=AERIAL_PURPLE, bg=AERIAL_BLUE)
        stuff_needed_head_lbl.pack(anchor=NW)

        stuff_needed_lbl = Label(self, text='          1. A DJI® Mavic Air / Mavic Pro Drone,\n'
                                            '          2. A suitable usb-connected DJI® Remote Control,\n'
                                            '          3. An Apple™ iPhone® with the DJI® Go 4.0 app installed.',
                                 font=(SYS_FONT, SECOND_HEADLINE_SIZE), bg=AERIAL_BLUE)
        stuff_needed_lbl.config(justify=LEFT)
        stuff_needed_lbl.pack(anchor=NW)

        stuff_img = PhotoImage(file='things-needed.gif')
        stuff_img_lbl = Label(self, image=stuff_img, bg=AERIAL_BLUE)
        stuff_img_lbl.photo = stuff_img
        stuff_img_lbl.pack()

        stuff_got_lbl = Label(self, text='\nDo you have all that we need?', font=(SYS_FONT, SECOND_HEADLINE_SIZE),
                              fg=AERIAL_PURPLE, bg=AERIAL_BLUE)
        stuff_got_lbl.pack()

        have_stuff_needed_btn = Button(self, text='Yes, Let\'s move on', font=(SYS_FONT, SECOND_HEADLINE_SIZE),
                                       fg=AERIAL_PURPLE, command=self.choose_server_screen)
        have_stuff_needed_btn.pack()

        aersoph_ls_logo_img = PhotoImage(file='aersoph_and_ls.gif')
        aersoph_ls_logo_lbl = Label(self, image=aersoph_ls_logo_img, bg=AERIAL_BLUE)
        aersoph_ls_logo_lbl.photo = aersoph_ls_logo_img
        aersoph_ls_logo_lbl.place(x=665, y=0)

        self.parent = parent

    def clear_frame(self):
        for widget in self.winfo_children():
            widget.destroy()

    def choose_server_screen(self):
        self.clear_frame()

        server_config_head_lbl = Label(self, text='Next, Server configuration', font=(SYS_FONT, HEADLINE_SIZE),
                                       fg=AERIAL_PURPLE, bg=AERIAL_BLUE)
        server_config_head_lbl.pack(anchor=NW)

        server_config_body_lbl = Label(self, text='\n       Real-Time Messaging Protocol (RTMP)\n'
                                                  '       was initially a proprietary protocol developed by Macromedia\n'
                                                  '       for streaming audio, video and data over the Internet,\n'
                                                  '       between a Flash player and a server.\n\n\n'
                                                  '              You have two options.\n'
                                                  '                     1. You can run the RTMP (Video Strem) Server locally*\n'
                                                  '                     2. You can run the server on another computer',
                                       font=(SYS_FONT, SECOND_HEADLINE_SIZE), bg=AERIAL_BLUE)
        server_config_body_lbl.config(justify=LEFT)
        server_config_body_lbl.pack()

        server_config_warning_lbl = Label(self, text='*(This option isn\'t recommended for old '
                                                     'or fairly weak computers)\n',
                                          font=(SYS_FONT, TEXT_SIZE), bg=AERIAL_BLUE)
        server_config_warning_lbl.pack()

        local_btn = Button(self, text="Run locally", font=(SYS_FONT, SECOND_HEADLINE_SIZE), fg=AERIAL_PURPLE,
                           command=self.run_locally_screen, width=10)
        local_btn.place(x=230, y=440)
        another_comp_btn = Button(self, text="Run remotely", font=(SYS_FONT, SECOND_HEADLINE_SIZE), fg=AERIAL_PURPLE,
                                  command=self.run_remotely_screen, width=10)
        another_comp_btn.place(x=430, y=440)

        aersoph_ls_logo_img = PhotoImage(file='aersoph_and_ls.gif')
        aersoph_ls_logo_lbl = Label(self, image=aersoph_ls_logo_img, bg=AERIAL_BLUE)
        aersoph_ls_logo_lbl.photo = aersoph_ls_logo_img
        aersoph_ls_logo_lbl.place(x=665, y=0)

    def get_stream_id(self):
        if self.get_local_ip() == "You aren't connected to an internet network.":
            return "connect to the internet to continue."
        else:
            lettersAndDigits = string.ascii_letters + string.digits
            return ''.join(random.choice(lettersAndDigits) for i in range(10))

    def get_local_ip(self):
        try:
            s = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
            s.connect(("8.8.8.8", 80))
            return s.getsockname()[0]
        except OSError:
            return "You aren't connected to an internet network."

    def run_locally_screen(self):
        self.clear_frame()

        run_locally_head_lbl = Label(self, text='Local RTMP server', font=(SYS_FONT, HEADLINE_SIZE),
                                     fg=AERIAL_PURPLE, bg=AERIAL_BLUE)
        run_locally_head_lbl.pack(anchor=NW)

        server_config_body_lbl = Label(self, text='\nThe server is installed as an instance of a docker image\n'
                                                  'called a container. if you don\'t have docker installed\n'
                                                  'on your machine, install it now, at docker.com.\n'
                                                  'just click this text to open the download landing page,\n'
                                                  'and get more information about docker.',
                                       font=(SYS_FONT, SECOND_HEADLINE_SIZE), bg=AERIAL_BLUE, cursor="hand2")
        server_config_body_lbl.config(justify=LEFT)
        server_config_body_lbl.pack()
        server_config_body_lbl.bind("<Button-1>", self.open_link)

        ip_address_is_lbl = Label(self, text='\n             Your IP address is:',
                                  font=(SYS_FONT, SECOND_HEADLINE_SIZE),
                                  fg=AERIAL_PURPLE, bg=AERIAL_BLUE)
        ip_address_is_lbl.config(justify=LEFT)
        ip_address_is_lbl.pack(anchor=NW)

        ip_address_lbl = Label(self, text=self.get_local_ip(), font=(IP_FONT, SECOND_HEADLINE_SIZE),
                               fg="black", bg=CODE_GRAY, borderwidth=2, relief="sunken")
        ip_address_lbl.pack()

        stream_id_is_lbl = Label(self, text='             Your streaming ID is:', font=(SYS_FONT, SECOND_HEADLINE_SIZE),
                                 fg=AERIAL_PURPLE, bg=AERIAL_BLUE)
        stream_id_is_lbl.config(justify=LEFT)
        stream_id_is_lbl.pack(anchor=NW)

        stream_id_lbl = Label(self, text=self.get_stream_id(), font=(IP_FONT, SECOND_HEADLINE_SIZE),
                              fg="black", bg=CODE_GRAY, borderwidth=2, relief="sunken")
        stream_id_lbl.pack()

        remember_to_save_lbl = Label(self, text='If you are running\nthe server remotely,\nsave this info!',
                                     font=(SYS_FONT, SECOND_HEADLINE_SIZE), bg=AERIAL_BLUE, cursor="hand2")
        remember_to_save_lbl.config(justify=LEFT)
        remember_to_save_lbl.place(x=520, y=330)

        refresh_btn = Button(self, text='Refresh', font=(SYS_FONT, SECOND_HEADLINE_SIZE),
                             fg=AERIAL_PURPLE, command=self.run_locally_screen)
        refresh_btn.place(x=20, y=490)

        move_on_btn = Button(self, text='OK, Let\'s move on', font=(SYS_FONT, SECOND_HEADLINE_SIZE),
                             fg=AERIAL_PURPLE, command=self.activate_server_screen)
        move_on_btn.place(x=570, y=490)

        aersoph_ls_logo_img = PhotoImage(file='aersoph_and_ls.gif')
        aersoph_ls_logo_lbl = Label(self, image=aersoph_ls_logo_img, bg=AERIAL_BLUE)
        aersoph_ls_logo_lbl.photo = aersoph_ls_logo_img
        aersoph_ls_logo_lbl.place(x=665, y=0)

    def run_remotely_screen(self):
        self.clear_frame()

        run_remotely_head_lbl = Label(self, text='Remote RTMP server', font=(SYS_FONT, HEADLINE_SIZE),
                                      fg=AERIAL_PURPLE, bg=AERIAL_BLUE)
        run_remotely_head_lbl.pack(anchor=NW)

        run_remotly_lbl = Label(self, text='\n       In order to install a remote RTMP server,\n'
                                           '       You\'ll need to open this software on the wanted computer,\n'
                                           '       and write the IP and streaming ID of that computer -\n'
                                           '       and insert them here. We will take care of autofill.\n'
                                           '       It\'s that easy!',
                                font=(SYS_FONT, SECOND_HEADLINE_SIZE), bg=AERIAL_BLUE)
        run_remotly_lbl.config(justify=LEFT)
        run_remotly_lbl.pack(anchor=NW)

        server_ip_lbl = Label(self, text="Server IP")
        server_ip_lbl.config(font=(SYS_FONT, HEADLINE_SIZE, "bold"), fg=AERIAL_PURPLE, bg=AERIAL_BLUE)
        server_ip_lbl.place(x=28, y=280)

        server_ip_entry = Entry(self, justify=CENTER, fg=AERIAL_PURPLE, bg=AERIAL_BLUE,
                                font=(SYS_FONT, SECOND_HEADLINE_SIZE))
        server_ip_entry.place(x=30, y=330)

        stream_id_lbl = Label(self, text="Stream ID")
        stream_id_lbl.config(font=(SYS_FONT, HEADLINE_SIZE, "bold"), fg=AERIAL_PURPLE, bg=AERIAL_BLUE)
        stream_id_lbl.place(x=420, y=280)

        stream_id_entry = Entry(self, justify=CENTER, fg=AERIAL_PURPLE, bg=AERIAL_BLUE,
                                font=(SYS_FONT, SECOND_HEADLINE_SIZE))
        stream_id_entry.place(x=420, y=330)

        space_lbl = Label(self, text="", fg=AERIAL_PURPLE, bg=AERIAL_BLUE)
        space_lbl.pack()

        activate_btn = Button(self, text='Activate!', font=(SYS_FONT, HEADLINE_SIZE + 10),
                              fg=AERIAL_PURPLE, command=self.activate_server_screen)
        activate_btn.place(x=530, y=440)

        aersoph_ls_logo_img = PhotoImage(file='aersoph_and_ls.gif')
        aersoph_ls_logo_lbl = Label(self, image=aersoph_ls_logo_img, bg=AERIAL_BLUE)
        aersoph_ls_logo_lbl.photo = aersoph_ls_logo_img
        aersoph_ls_logo_lbl.place(x=665, y=0)

    def open_link(self, event):
        webbrowser.open_new(r'https://www.docker.com/products/docker-desktop')

    def activate_server_screen(self):
        self.clear_frame()

        activate_server_head_lbl = Label(self, text='Activating the server', font=(SYS_FONT, HEADLINE_SIZE),
                                         fg=AERIAL_PURPLE, bg=AERIAL_BLUE)
        activate_server_head_lbl.pack(anchor=NW)

        activate_server_lbl = Label(self, text='       If you installed docker,\n'
                                               '       please write us your name and then click "Activate".\n\n'
                                               '       It\'s THAT easy!\n'
                                               '       We hope everything is clear and wish you an easy use of Aerial!',
                                    font=(SYS_FONT, SECOND_HEADLINE_SIZE), bg=AERIAL_BLUE)
        activate_server_lbl.config(justify=LEFT)
        activate_server_lbl.pack(anchor=NW)

        name_lbl = Label(self, text='\n        Please enter your name:',
                         font=(SYS_FONT, SECOND_HEADLINE_SIZE),
                         fg=AERIAL_PURPLE, bg=AERIAL_BLUE)
        name_lbl.config(justify=LEFT)
        name_lbl.pack(anchor=NW)

        name_entry = Entry(self, justify=CENTER, fg=AERIAL_PURPLE, bg=AERIAL_BLUE, font=(SYS_FONT, HEADLINE_SIZE))
        name_entry.config(width=10)
        name_entry.place(x=80, y=310)

        activate_btn = Button(self, text='Activate!', font=(SYS_FONT, HEADLINE_SIZE + 10),
                              fg=AERIAL_PURPLE, command=self.activate_server_screen)
        activate_btn.place(x=500, y=400)

        aersoph_ls_logo_img = PhotoImage(file='aersoph_and_ls.gif')
        aersoph_ls_logo_lbl = Label(self, image=aersoph_ls_logo_img, bg=AERIAL_BLUE)
        aersoph_ls_logo_lbl.photo = aersoph_ls_logo_img
        aersoph_ls_logo_lbl.place(x=665, y=0)


class MainScreen(Frame):
    def __init__(self, parent, referer):
        root.geometry(MAIN_WINDOW_DIMENSIONS)
        center(root)
        try:
            referer.destroy()
        except:
            pass

        Frame.__init__(self, parent, bg=AERIAL_BLUE)

        hello_name_lbl = Label(self, text='Good Evening, Yair', font=(SYS_FONT, HEADLINE_SIZE),
                               fg=AERIAL_PURPLE, bg=AERIAL_BLUE)
        hello_name_lbl.pack(anchor=NW)

        start_stream_frm = Frame(self, bg=AERIAL_BLUE, highlightbackground=AERIAL_PURPLE, highlightthickness=3,
                                 width=250, height=100, bd=0)
        start_stream_frm.place(x=10, y=100)

        take_snapshot_frm = Frame(self, bg=AERIAL_BLUE, highlightbackground=AERIAL_PURPLE, highlightthickness=3,
                                  width=250, height=100, bd=0)
        take_snapshot_frm.place(x=10, y=210)

        mission_timer_lbl = Label(self, text='Detection Log', font=(SYS_FONT, SECOND_HEADLINE_SIZE), bg=AERIAL_BLUE,
                                  fg=AERIAL_PURPLE)
        mission_timer_lbl.place(x=350, y=500)

        detection_log_frm = Frame(self, bg=AERIAL_BLUE, highlightbackground=AERIAL_PURPLE, highlightthickness=3,
                                  width=650, height=100, bd=0)
        detection_log_frm.place(x=350, y=540)

        mission_timer_lbl = Label(self, text='Mission Timer', font=(SYS_FONT, SECOND_HEADLINE_SIZE), bg=AERIAL_BLUE,
                                  fg=AERIAL_PURPLE)
        mission_timer_lbl.place(x=10, y=320)

        zero_timer = Label(self, text='00:00', font=(SYS_FONT, TIMER_SIZE), bg=AERIAL_BLUE,
                           fg=AERIAL_PURPLE)
        zero_timer.place(x=10, y=350)

        not_live__lbl = Label(self, text='Not Live', font=(SYS_FONT, SECOND_HEADLINE_SIZE), bg=AERIAL_BLUE,
                              fg=AERIAL_PURPLE)
        not_live__lbl.place(x=1040, y=30)

        start_stream_btn = Button(start_stream_frm, text="Go Live", fg=AERIAL_PURPLE,
                                  font=(SYS_FONT, SECOND_HEADLINE_SIZE),
                                  command=lambda: TkStream(self, start_stream_btn, not_live__lbl, detection_log_frm,
                                                           zero_timer))
        start_stream_btn.place(x=70, y=30)

        icon_img = PhotoImage(file='icon.gif')
        icon_img = icon_img.subsample(3)
        icon_lbl = Label(self, image=icon_img, bg=AERIAL_BLUE)
        icon_lbl.photo = icon_img
        icon_lbl.place(x=0, y=470)

        aersoph_ls_logo_img = PhotoImage(file='aersoph_and_ls.gif')
        aersoph_ls_logo_lbl = Label(self, image=aersoph_ls_logo_img, bg=AERIAL_BLUE)
        aersoph_ls_logo_lbl.photo = aersoph_ls_logo_img
        aersoph_ls_logo_lbl.place(x=160, y=550)

        self.parent = parent

    def clear_frame(self):
        for widget in self.winfo_children():
            widget.destroy()


def center(win):
    win.update_idletasks()
    width = win.winfo_width()
    height = win.winfo_height()
    x = (win.winfo_screenwidth() // 2) - (width // 2)
    y = (win.winfo_screenheight() // 2) - (height // 2)
    win.geometry('{}x{}+{}+{}'.format(width, height, x, y))


if __name__ == "__main__":
    # Opening window
    root = Tk()
    root.title('Aerial for macOS')
    icon = Image('photo', file='icon.gif')
    root.call('wm', 'iconphoto', root._w, icon)
    root.geometry(WINDOW_MIN_DIMENSIONS)
    center(root)

    # Determining the net settings
    options = {
        'model': 'cfg/yolo.cfg',
        'load': 'bin/yolo.weights',
        'threshold': 0.2,
        'gpu': 1.0
    }

    # Creating the net according to the settings
    tfnet = TFNet(options)

    WelcomeScreen(root).pack(fill="both", expand=True)
    root.mainloop()
